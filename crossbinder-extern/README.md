# Crossbinder Extern

A lifecycle interceptor that enables direct injection of external objects into entities managed by
Crossbinder. Further details on setup and integration are available from the project page at
[http://www.hashvoid.com/crossbinder](http://www.hashvoid.com/crossbinder)
