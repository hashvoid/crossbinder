/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.hashvoid.crossbinder.hod.support.binder;

import java.util.List;

import com.hashvoid.crossbinder.hod.ConfigurationProvider;
import com.hashvoid.crossbinder.hod.Injector;
import com.hashvoid.crossbinder.hod.Locator;

/**
 * @author randondiesel
 *
 */

public interface BinderContext {

	Locator getLocator();

	Injector getInjector();

	EventHandler getEventHandler();

	List<ConfigurationProvider> getConfigurationProviders();
}
