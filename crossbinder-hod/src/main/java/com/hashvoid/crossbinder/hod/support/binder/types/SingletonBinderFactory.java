/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.hashvoid.crossbinder.hod.support.binder.types;

import java.util.logging.Logger;

import com.hashvoid.crossbinder.hod.LifecycleInterceptor;
import com.hashvoid.crossbinder.hod.MethodInterceptor;
import com.hashvoid.crossbinder.hod.Prototype;
import com.hashvoid.crossbinder.hod.Provider;
import com.hashvoid.crossbinder.hod.support.binder.Binder;
import com.hashvoid.crossbinder.hod.support.binder.BinderContext;

/**
 * @author randondiesel
 *
 */

public class SingletonBinderFactory {

	private static final Logger LOGGER = Logger.getLogger(SingletonBinderFactory.class.getName());

	public Binder createBinder(Class<?> implCls, BinderContext ctxt) {

		if(!BinderValidations.checkInstantiable(implCls)) {
			LOGGER.warning(String.format("not_instantiable, class=%s", implCls.getName()));
			return null;
		}
		if(!BinderValidations.checkNotAnnotatedWith(implCls, Provider.class, Prototype.class)) {
			LOGGER.warning(String.format("conflicting_annotations, class=%s", implCls.getName()));
			return null;
		}
		if(!BinderValidations.checkNotSubtypeOf(implCls, LifecycleInterceptor.class, MethodInterceptor.class)) {
			return null;
		}

		return new SingletonBinder(implCls, ctxt);

		//This code is no longer needed as we now support shadow singletons.
		/*
		SingletonBinder binder = new SingletonBinder(implCls, ctxt);
		if(binder.getInterfaceTypes().isEmpty()) {
			LOGGER.warning(String.format("no_bindable_interfaces, class=%s", implCls.getName()));
			return null;
		}

		return binder;
		*/
	}
}
